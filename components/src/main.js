import { createApp } from 'vue';
import DashboardHoliday from './components/DashboardHoliday.vue';

const holidayApp = createApp(DashboardHoliday, { type: 'holiday' });
holidayApp.mount('#holiday-list');

const leaveApp = createApp(DashboardHoliday, { type: 'leave' });
leaveApp.mount('#leave-details');
