// src/router/index.js
import { createRouter, createWebHistory } from 'vue-router';
// import LeaveRequest from '../components/LeaveRequest.vue';
// import UpcomingLeaves from '../components/UpcomingLeaves.vue';

const routes = [
    // {
    // path: '/',
    // redirect: '/leave-request' 
    // },
    // {
    //     path: '/leave-request',
    //     name: 'LeaveRequest',
    //     component: LeaveRequest,
    // },
    // {
    //     path: '/upcoming-leaves',
    //     name: 'UpcomingLeaves',
    //     component: UpcomingLeaves,
    // }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
